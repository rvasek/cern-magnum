# ChangeLog

All notable changes to this project are documented in this file.

## [0.7.0] - 2021-03-02

- Update eosxd helm chart to 0.2.7

## [0.5.0] - 2020-07-10

- Add base 0.1.0 disabled by default  for cern-keytab and cern-hostcert.

## [0.4.0] - 2020-06-26

- Add openstack-cinder-csi 1.1.1 disabled by default

## [0.3.0] - 2020-05-27

- Add requirement on fluentd with a set of default values
- Add requirement on landb-sync
- Add requirement on prometheus-cern

## [0.1.1] - 2020-05-06

- Update nvidia-gpu requirement to 0.3.0
- Fix gitlab-ci with appropriate cern repo

## [0.1.0] - 2020-04-30

- First Release
